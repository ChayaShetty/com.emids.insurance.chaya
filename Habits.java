package com.emids.insurance.person.details;

import com.emids.insurance.conditions.PersonConditions;

/**
 * Class to set different habits of a person
 * @author emidstest02
 *
 */
public class Habits extends PersonConditions{
	
	public Habits(boolean smoking, boolean alcohol, boolean dailyExercise, boolean drugs) {
		super(smoking, alcohol, dailyExercise, drugs);
	}
	

}
