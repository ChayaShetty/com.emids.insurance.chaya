package com.emids.insurance.conditions;

public class PersonConditions {

	private boolean state1 = false;
	private boolean state2 = false;
	private boolean state3 = false;
	private boolean state4 = false;
	
	public PersonConditions(boolean state1, boolean state2, boolean state3, boolean state4) {
		super();
		this.state1 = state1;
		this.state2 = state2;
		this.state3 = state3;
		this.state4 = state4;
	}

	public boolean isState1() {
		return state1;
	}

	public void setState1(boolean state1) {
		this.state1 = state1;
	}

	public boolean isState2() {
		return state2;
	}

	public void setState2(boolean state2) {
		this.state2 = state2;
	}

	public boolean isState3() {
		return state3;
	}

	public void setState3(boolean state3) {
		this.state3 = state3;
	}

	public boolean isState4() {
		return state4;
	}

	public void setState4(boolean state4) {
		this.state4 = state4;
	}
	
	
}
