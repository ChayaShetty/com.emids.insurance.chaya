package com.emids.insurance.person.details;

import com.emids.insurance.conditions.PersonConditions;

/**
 * Class to set different health conditions for a person
 * @author emidstest02
 *
 */
public class CurrentHealth extends PersonConditions{
		
	public CurrentHealth(boolean hyperTension, boolean bloodPressure, boolean bloodSugar, boolean overweight) {
		super(hyperTension, bloodPressure, bloodSugar, overweight);
	}
	
}
