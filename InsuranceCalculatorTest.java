package com.emids.insurance.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.emids.insurance.calculator.InsuranceCalculator;
import com.emids.insurance.person.details.CurrentHealth;
import com.emids.insurance.person.details.Habits;
import com.emids.insurance.person.details.Person;
/*
 * Test class for InsuranceCalculator.java
 */
class InsuranceCalculatorTest {

	@Test
	void testGetInsuranceAmout() {
		//case 1
		CurrentHealth currentHealth = new CurrentHealth(false, false
				, false, false);
		Habits habits = new Habits(false, false, false, false);
		Person person = new Person("xyz", "abc", "female", 10, 
				currentHealth, habits);
		assertNotNull(person);
		InsuranceCalculator insuranceCalculator = new InsuranceCalculator(person);
		assertEquals(5000, insuranceCalculator.getInsuranceAmout());


		//case 2
		CurrentHealth currentHealth2 = new CurrentHealth(false, false
				, false, false);
		Habits habits2 = new Habits(false, false, false, false);
		Person person2 = new Person("xyz", "abc", "male", 18, 
				currentHealth2, habits2);
		assertNotNull(person2);
		InsuranceCalculator insuranceCalculator2 = new InsuranceCalculator(person2);
		assertEquals(5100, insuranceCalculator2.getInsuranceAmout());

		//case 3
		CurrentHealth currentHealth3 = new CurrentHealth(false, false
				, false, true);
		Habits habits3 = new Habits(false, true, true, false);
		Person person3 = new Person("Norman", "Gomes", "male", 34, 
				currentHealth3, habits3);
		assertNotNull(person3);
		InsuranceCalculator insuranceCalculator3 = new InsuranceCalculator(person3);
		assertEquals(11872, insuranceCalculator3.getInsuranceAmout());



	}

}
