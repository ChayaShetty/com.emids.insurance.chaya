package com.emids.insurance.person.details;

import com.emids.insurance.conditions.PersonConditions;

/**
 * Person class to store person specific details
 * @author emidstest02
 *
 */
public class Person {
	private String firstName;
	private String lastName;
	private String gender;
	private int age;
	private PersonConditions currentHealth;
	private PersonConditions habits;
	
	public Person(String firstName, String lastName, String gender, int age, PersonConditions currentHealth, PersonConditions habits) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		initializeCurrentHealth(currentHealth);
		initializeHabits(habits);
	}
	
	private void initializeCurrentHealth(PersonConditions currHealth) {
		currentHealth = new CurrentHealth(currHealth.isState1(), currHealth.isState2(), currHealth.isState3(), currHealth.isState4());				
	}
	
	private void initializeHabits(PersonConditions habitsObj) {
		habits = new Habits(habitsObj.isState1(), habitsObj.isState2(), habitsObj.isState3(), habitsObj.isState4());
			
	}	

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public PersonConditions getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}

	public PersonConditions getHabits() {
		return habits;
	}

	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
	

}
