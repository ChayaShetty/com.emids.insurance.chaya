package com.emids.insurance.calculator;

import com.emids.insurance.person.details.Person;

/**
 * Class which calculates the insurance of a person based on inputs provided
 * and different business logic
 * 
 * @author emidstest02
 *
 */
public class InsuranceCalculator {
	
	private final int basePremium = 5000;
	private final int baseAge = 18;
	private final int increaseAfterYear = 5;
	private Person person; 
	private long totalInsuranceAmount = basePremium;
	
	public InsuranceCalculator(Person person) {
		initializePerson(person);
	}
	
	private void initializePerson(Person person) {
		this.person = new Person(person.getFirstName(), person.getLastName(), person.getGender(), person.getAge(),
				person.getCurrentHealth(), person.getHabits());
	}
	
	/**
	 * Method to calculate the total insurance amount based on different criteria
	 * like age, gender, habits, health conditions
	 * @return total insurance amount
	 */
	public long getInsuranceAmout() {
		totalInsuranceAmount += getInsuranceAmtByAge();
		totalInsuranceAmount += getInsuranceAmtByGender();
		totalInsuranceAmount += getInsuranceAmtByHealthConditions(totalInsuranceAmount);
		totalInsuranceAmount += getInsuranceAmtByHabits(totalInsuranceAmount);
		return totalInsuranceAmount;
	}

	/**
	 * Method which calculates insurance amount based on habits of a person
	 * @param insuranceAmt
	 * @return insurance amount by habits
	 */
	private long getInsuranceAmtByHabits(long insuranceAmt) {
		long amount = 0;
		if(this.person.getHabits().isState2()) {
			amount += insuranceAmt * .03f;
		}
		if(this.person.getHabits().isState3()) {
			amount += insuranceAmt * .03f;
		}
		if(this.person.getHabits().isState4()) {
			amount += insuranceAmt * .03f;
		}
		if(this.person.getHabits().isState1()) {
			amount -= (insuranceAmt * .03f);
		}
		return amount;
	}
	
	/**
	 * Method which returns insurance amount based on person's health conditions
	 * @param insuranceAmt
	 * @return insurance amount by health conditions
	 */
	private long getInsuranceAmtByHealthConditions(long insuranceAmt) {
		long amount = 0;
		if(this.person.getCurrentHealth().isState1()) {
			amount += insuranceAmt * .01f;
		}
		if(this.person.getCurrentHealth().isState2()) {
			amount += insuranceAmt * .01f;
		}
		if(this.person.getCurrentHealth().isState3()) {
			amount += insuranceAmt * .01f;
		}
		if(this.person.getCurrentHealth().isState4()) {
			amount += insuranceAmt * .01f;
		}
		
		return amount;
	}
	
	/**
	 * Method to calculate insurance amount based on gender
	 * @return insurance amt by gender
	 */
	private long getInsuranceAmtByGender() {
		if(this.person.getGender().equalsIgnoreCase("male")) {
			return (long) (basePremium * .02f);
		}
		return 0;
	}
	
	/**
	 * Method to calculate insurance amount based on age
	 * @return insurance amt by age
	 */
	private long getInsuranceAmtByAge() {
		long insuranceAmount = 0;

		if(this.person.getAge() > 18) {
		if(this.person.getAge() <= 40) {
			int tempAge = this.person.getAge() - this.baseAge;
			insuranceAmount += calculateInsuranceRecursivelyBasedOnAge(tempAge, .10f);
		}
		else if(this.person.getAge()>40) {
			int tempAge = 40 - this.baseAge;
			insuranceAmount += calculateInsuranceRecursivelyBasedOnAge(tempAge, .10f);
			
			int tempAge2 = this.person.getAge() - 40;
			insuranceAmount += calculateInsuranceRecursivelyBasedOnAge(tempAge2, .20f);
		}
		}
		return insuranceAmount;
	}
	
	/**
	 * Method which calculates insurance amount based on age recursively
	 * i.e it first calculates the no. of times the insurance has to be increased
	 * based on age factor and recursively adds the amount
	 * @param age
	 * @param percentage
	 * @return insurance amount
	 */
	private long calculateInsuranceRecursivelyBasedOnAge(int age, float percentage) {
		int insuranceAmount = basePremium;
		int frequency = age / increaseAfterYear;
		for(int i=0;i<frequency;i++) {
			insuranceAmount += insuranceAmount * percentage;
		}
		
		return insuranceAmount;
	}
	
}
